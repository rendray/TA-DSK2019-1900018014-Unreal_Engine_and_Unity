.model small
.code
org 100h


mulai   : jmp proses
        pertanyaan db 'Masukkan umur Anda : $'
        kal1 db 10,'Anda masih anak-anak $'
        kal2 db 10,'Anda msih balita $'
        kal3 db 10,'Anda masih remaja $'
        kal4 db 10,'Anda sudah dewasa $'

proses  :
        lea dx,pertanyaan
        mov ah,9h
        int 21h

        mov ah,0
        int 16h
        push ax

        pop ax
        mov dl,al
        mov ah,2
        int 21h

        cmp al,'1'
        jbe balita

        cmp al,'4'
        ja dewasa

        cmp al,'3'
        ja remaja

        cmp al,'2'
        ja anak

balita   :
        lea dx,kal2
        mov ah,9h
        int 21h
        jmp exit

anak    :
        lea dx,kal1
        mov ah,9h
        int 21h
        jmp exit

dewasa  :
        lea dx,kal4
        mov ah,9h
        int 21h
        jmp exit

remaja  :
        lea dx,kal3
        mov ah,9h
        int 21h
        jmp exit

exit    :int 20h       
end     mulai